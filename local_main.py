from PIL import Image
import datetime as dt
from io import BytesIO
from gather_radar_images import (download_latest_image, radars)

class Logger:

    def log(self, msg):
        print(msg)

global_logger = Logger()


def main():
    image_data = download_latest_image(utcnow=dt.datetime.utcnow(), radar_prefix=radars['Sydney128k'], logger=global_logger)
    img = Image.open(BytesIO(image_data))
    img.show()

if __name__ == '__main__':
    main()